import greenfoot.*;

/**
 * EnvironmentObject
 * 
 * @author Project Group 13
 * @version 2012-01-09
 */
public abstract class EnvironmentObject extends Actor {
    /**
     * the Cartesian position x-coordinate
     */
    protected int posX = 0;
    /**
     * the Cartesian position y-coordinate
     */
    protected int posY = 0;

    /**
     * the velocity x-component
     */
    protected int vX = 0;
    /**
     * the velocity y-component
     */
    protected int vY = 0;

    /**
     * current image
     */
    protected int imageNum = 0;
    /**
     * image file
     */
    protected String imageFileName;
    protected String imageFileName1;
    protected String imageFileName_r;
    protected String imageFileName1_r;

    /**
     * Class constructor. (4 parameters)
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     * @param imageFileName_r
     *            image file
     * @param imageFileName1_r
     *            image file
     */
    public EnvironmentObject(String imageFileName, String imageFileName1,
            String imageFileName_r, String imageFileName1_r) {
        this.imageFileName = imageFileName;
        this.imageFileName1 = imageFileName1;
        this.imageFileName_r = imageFileName_r;
        this.imageFileName1_r = imageFileName1_r;

        setImage(imageFileName);
    }

    /**
     * Class constructor. (2 parameters)
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     */
    public EnvironmentObject(String imageFileName, String imageFileName1) {
        this.imageFileName = imageFileName;
        this.imageFileName1 = imageFileName1;

        setImage(imageFileName);
    }

    /**
     * Acts.
     * 
     * @see greenfoot.Actor#act()
     */
    @Override
    public void act() {
        // renew the position by the velocity
        posX += vX * 1;
        posY += vY * 1;
    }

    /**
     * Gets the Cartesian position x-coordinate.
     * 
     * @return x-coordinate
     */
    public final int getPosX() {
        return posX;
    }

    /**
     * Sets the Cartesian position x-coordinate.
     * 
     * @param posX
     *            x-coordinate
     */
    public final void setPosX(int posX) {
        this.posX = posX;
    }

    /**
     * Gets the Cartesian position y-coordinate.
     * 
     * @return y-coordinate
     */
    public final int getPosY() {
        return posY;
    }

    /**
     * Sets the Cartesian position y-coordinate.
     * 
     * @param posY
     *            y-coordinate
     */
    public final void setPosY(int posY) {
        this.posY = posY;
    }

    /**
     * Sets the Cartesian position.
     * 
     * @param posX
     *            x-coordinate
     * @param posY
     *            y-coordinate
     */
    public final void setPos(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    /**
     * Gets the velocity x-component.
     * 
     * @return x-component
     */
    public final int getVX() {
        return vX;
    }

    /**
     * Sets the velocity x-component.
     * 
     * @param vX
     *            x-component
     */
    public final void setVX(int vX) {
        this.vX = vX;
    }

    /**
     * Gets the velocity y-component.
     * 
     * @return y-component
     */
    public final int getVY() {
        return vY;
    }

    /**
     * Sets the velocity y-component.
     * 
     * @param vY
     *            y-component
     */
    public final void setVY(int vY) {
        this.vY = vY;
    }

    /**
     * Sets the velocity.
     * 
     * @param vX
     *            x-component
     * @param vY
     *            y-component
     */
    public final void setV(int vX, int vY) {
        this.vX = vX;
        this.vY = vY;
    }

    /**
     * Gets the image file pathname.
     * 
     * @return image file
     */
    public final String getImageFileName() {
        return imageFileName;
    }

}
