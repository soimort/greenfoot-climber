import greenfoot.*;

/**
 * Background
 * 
 * @author Project Group 13
 * @version 2012-01-09
 */
public abstract class Background extends Actor {
    /**
     * the width of the background
     */
    protected int width;
    /**
     * the height of the background
     */
    protected int height;
    /**
     * image file
     */
    protected String imageFileName;

    /**
     * Class constructor.
     * 
     * @param imageFileName
     *            image file
     * @param width
     *            width of the background
     * @param height
     *            height of the background
     */
    public Background(String imageFileName, int width, int height) {
        this.imageFileName = imageFileName;
        setImage(imageFileName);

        this.width = width;
        this.height = height;
    }

    /**
     * Gets the width of the background.
     * 
     * @return width of the background
     */
    public final int getWidth() {
        return width;
    }

    /**
     * Gets the height of the background.
     * 
     * @return height of the background
     */
    public final int getHeight() {
        return height;
    }

    /**
     * Gets the image file pathname.
     * 
     * @return image file
     */
    public final String getImageFileName() {
        return imageFileName;
    }

}
