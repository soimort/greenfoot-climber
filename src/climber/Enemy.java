/**
 * Enemy
 * 
 * @author Project Group 13
 * @version 2012-01-09
 * @see EnvironmentObject
 */
public abstract class Enemy extends EnvironmentObject {
    /**
     * Class constructor. (4 parameters)
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     * @param imageFileName_r
     *            image file
     * @param imageFileName1_r
     *            image file
     */
    public Enemy(String imageFileName, String imageFileName1,
            String imageFileName_r, String imageFileName1_r) {
        super(imageFileName, imageFileName1, imageFileName_r, imageFileName1_r);
    }

    /**
     * Class constructor. (2 parameters)
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     */
    public Enemy(String imageFileName, String imageFileName1) {
        super(imageFileName, imageFileName1);
    }

    /**
     * Acts.
     * 
     * @see greenfoot.Actor#act()
     * @see EnvironmentObject#act()
     */
    @Override
    public void act() {
        super.act();
    }

}
