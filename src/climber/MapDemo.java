/**
 * MapDemo
 * 
 * @author Project Group 13
 * @version 2012-01-10
 * @see MapTemplate
 */
public final class MapDemo extends MapTemplate {
    /**
     * map ID
     */
    static final String mapId = "map_demo";

    /**
     * image file of the background
     */
    static final String backgroundImage = "sky.png";
    /**
     * image file of the main background
     */
    static final String mainBackgroundImage = "main.png";
    /**
     * the width of the world
     */
    static final int mainBackgroundWidth = 600;
    /**
     * the height of the world
     */
    static final int mainBackgroundHeight = 2000;
    /**
     * image file of the player (main character)
     */
    static final String playerImage = "player.png";

    /**
     * image file of platforms
     */
    static final String platformImage = "platform.png";
    /**
     * the positions of platforms
     */
    static final int[][] platforms = { { 300, 1920 }, { 240, 1920 },
            { 180, 1920 }, { 120, 1920 }, { 60, 1920 }, { 360, 1920 },
            { 420, 1920 }, { 480, 1920 }, { 540, 1920 }, { 30, 1830 },
            { 90, 1830 }, { 350, 1830 }, { 410, 1830 }, { 470, 1830 },
            { 100, 1760 }, { 250, 1760 }, { 350, 1640 }, { 410, 1640 },
            { 470, 1640 }, { 250, 1540 }, { 190, 1540 }, { 400, 1450 },
            { 460, 1450 }, { 560, 1450 }, { 240, 1360 }, { 300, 1360 },
            { 330, 1360 }, { 390, 1360 }, { 60, 1280 }, { 280, 1280 },
            { 340, 1280 }, { 100, 1210 }, { 160, 1210 }, { 440, 1210 },
            { 500, 1210 }, { 270, 1140 }, { 250, 1040 }, { 360, 1040 },
            { 240, 900 }, { 300, 900 }, { 360, 900 }, { 420, 900 },
            { 60, 780 }, { 120, 780 }, { 180, 780 }, { 240, 780 },
            { 300, 700 }, { 250, 600 }, { 350, 600 }, { 200, 500 },
            { 400, 500 }, { 150, 400 }, { 450, 400 }, { 200, 300 },
            { 400, 300 }, { 250, 200 }, { 350, 200 } };

    /**
     * image file of birdnests
     */
    static final String birdnestImage = "enemy_birdnest.png";
    static final String birdnestImage1 = "enemy_birdnest1.png";
    /**
     * the positions of birdnests
     */
    static final int[][] birdnests = { { 340, 1700 }, { 80, 1500 },
            { 100, 1080 }, { 400, 730 }, { 340, 530 }, { 250, 300 }, };

    /**
     * image file of birds
     */
    static final String birdImage = "enemy_bird.png";
    static final String birdImage1 = "enemy_bird1.png";
    static final String birdImage_r = "enemy_bird_r.png";
    static final String birdImage1_r = "enemy_bird1_r.png";
    /**
     * the positions of birds
     */
    static final int[][] birds = { { 120, 1640 }, { 580, 1240 }, { 40, 1000 },
            { 300, 700 }, { 120, 350 }, { 560, 80 } };

    /**
     * image file of lizards
     */
    static final String lizardImage = "enemy_lizard.png";
    static final String lizardImage1 = "enemy_lizard1.png";
    static final String lizardImage_r = "enemy_lizard_r.png";
    static final String lizardImage1_r = "enemy_lizard1_r.png";
    /**
     * the positions of lizards
     */
    static final int[][] lizards = { { 150, 1000 }, { 240, 540 }, { 300, 480 } };

    /**
     * image file of stones
     */
    static final String stoneImage = "enemy_stone.png";
    static final String stoneImage1 = "enemy_stone1.png";
    /**
     * the positions of stones
     */
    static final int[][] stones = { { 450, 1700 }, { 300, 1500 },
            { 200, 1100 }, { 540, 1000 }, { 240, 700 }, };

    /**
     * image file of coins
     */
    static final String coinImage = "coin.png";
    /**
     * the positions of coins
     */
    static final int[][] coins = { { 420, 1820 }, { 440, 1820 }, { 420, 1620 },
            { 440, 1620 }, { 320, 1120 }, { 340, 1120 }, { 420, 800 },
            { 440, 800 }, { 250, 500 }, { 280, 500 }, { 310, 500 },
            { 350, 250 }, { 370, 250 }, };

    /**
     * Initializes the map.
     * 
     * @param world
     *            world of the game
     */
    public final void init(ClimberWorld world) {
        world.setBackground(getFileName(mapId, backgroundImage));
        world.setMainBackground(getFileName(mapId, mainBackgroundImage),
                mainBackgroundWidth, mainBackgroundHeight);

        world.setPlayer(getFileName(mapId, playerImage));

        world.setPlatforms(getFileName(mapId, platformImage), platforms);

        world.setBirdnests(getFileName(mapId, birdnestImage),
                getFileName(mapId, birdnestImage1), birdnests);
        world.setBirds(getFileName(mapId, birdImage),
                getFileName(mapId, birdImage1),
                getFileName(mapId, birdImage_r),
                getFileName(mapId, birdImage1_r), birds);
        world.setLizards(getFileName(mapId, lizardImage),
                getFileName(mapId, lizardImage1),
                getFileName(mapId, lizardImage_r),
                getFileName(mapId, lizardImage1_r), lizards);
        world.setStones(getFileName(mapId, stoneImage),
                getFileName(mapId, stoneImage1), stones);

        world.setCoins(getFileName(mapId, coinImage), coins);

    }

}
