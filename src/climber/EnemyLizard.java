/**
 * EnemyLizard
 * 
 * @author Project Group 13
 * @version 2012-01-10
 * @see Enemy
 */
public final class EnemyLizard extends Enemy {
    /**
     * timer for animation
     */
    private int timer = 0;

    /**
     * Class constructor.
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     * @param imageFileName_r
     *            image file
     * @param imageFileName1_r
     *            image file
     * @param posX
     *            initial x-coordinate
     * @param posY
     *            initial y-coordinate
     */
    public EnemyLizard(String imageFileName, String imageFileName1,
            String imageFileName_r, String imageFileName1_r, int posX, int posY) {
        super(imageFileName, imageFileName1, imageFileName_r, imageFileName1_r);
        setPos(posX, posY);

        vY = -1;
    }

    /**
     * Class constructor. (sets the position by default)
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     * @param imageFileName_r
     *            image file
     * @param imageFileName1_r
     *            image file
     */
    public EnemyLizard(String imageFileName, String imageFileName1,
            String imageFileName_r, String imageFileName1_r) {
        super(imageFileName, imageFileName1, imageFileName_r, imageFileName1_r);
        setPos(0, 0);

        vY = -1;
    }

    /**
     * Acts.
     * 
     * @see greenfoot.Actor#act()
     * @see Enemy#act()
     */
    @Override
    public void act() {
        super.act();

        timer++;
        if (timer == 10) {
            if (imageNum == 0) {
                setImage(imageFileName1);
                imageNum = 1;
            } else if (imageNum == 1) {
                setImage(imageFileName);
                imageNum = 0;
            } else if (imageNum == 2) {
                setImage(imageFileName1_r);
                imageNum = 3;
            } else if (imageNum == 3) {
                setImage(imageFileName_r);
                imageNum = 2;
            }
            timer = 0;
        }

        if (posY > 2000) {
            vY = -vY;
            setImage(imageFileName);
            imageNum = 0;
        } else if (posY < 300) {
            vY = -vY;
            setImage(imageFileName_r);
            imageNum = 2;
        }
    }

    /**
     * Triggers the collision event.
     * 
     * @param player
     *            player
     */
    public static void triggerEvent(Player player) {
        player.setLife(player.getLife());
        player.setStamina(player.getStamina() - 2);
        player.setScore(player.getScore());

        player.startMessageTimer();
        player.setMessage("You hit a lizard. Stamina lost!");
    }
}
