/**
 * EnemyStone
 * 
 * @author Project Group 13
 * @version 2012-01-09
 * @see Enemy
 */
public final class EnemyStone extends Enemy {
    /**
     * Class constructor.
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     * @param posX
     *            initial x-coordinate
     * @param posY
     *            initial y-coordinate
     */
    public EnemyStone(String imageFileName, String imageFileName1, int posX,
            int posY) {
        super(imageFileName, imageFileName1);
        setPos(posX, posY);

        vY = 4;
    }

    /**
     * Class constructor. (sets the position by default)
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     */
    public EnemyStone(String imageFileName, String imageFileName1) {
        super(imageFileName, imageFileName1);
        setPos(0, 0);

        vY = 4;
    }

    /**
     * Acts.
     * 
     * @see greenfoot.Actor#act()
     * @see Enemy#act()
     */
    @Override
    public void act() {
        super.act();

        if (posY > 2000)
            posY = 0;
    }

    /**
     * Triggers the collision event.
     * 
     * @param player
     *            player
     */
    public static void triggerEvent(Player player) {
        player.setLife(player.getLife() - 2);
        player.setStamina(player.getStamina() - 2);
        player.setScore(player.getScore());

        player.startMessageTimer();
        player.setMessage("You hit a stone. Life and stamina lost!");
    }
}
