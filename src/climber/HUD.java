import greenfoot.*;
import java.awt.Color;
import java.awt.Font;

/**
 * HUD
 * 
 * @author Project Group 13
 * @version 2012-01-09
 */
public class HUD extends Actor {
    /**
     * the width of the HUD
     */
    private static int width = 120;
    /**
     * the height of the HUD
     */
    private static int height = 100;
    /**
     * font size of the HUD
     */
    private static float fontSize = 12.0f;

    /**
     * Constructor.
     * 
     * @param message
     *            message
     * @param score
     *            life
     * @param score
     *            stamina
     * @param score
     *            score
     */
    public HUD(String message, int life, int stamina, int score) {
        update(message, life, stamina, score);
    }

    /**
     * Constructor. (by default)
     */
    public HUD() {
        update("Player", 100, 100, 0);
    }

    /**
     * Updates the HUD.
     * 
     * @param message
     *            message
     * @param score
     *            score
     */
    public void update(String message, int life, int stamina, int score) {
        GreenfootImage imageHUD = new GreenfootImage(width, height);
        imageHUD.setColor(new Color(0, 0, 0, 128));
        imageHUD.fillRect(5, 5, width - 10, height - 10);

        Font font = imageHUD.getFont();
        font = font.deriveFont(fontSize);
        imageHUD.setFont(font);
        imageHUD.setColor(Color.WHITE);
        imageHUD.drawString(message, 20, 20);
        imageHUD.drawString("Life: " + life, 20, 50);
        imageHUD.drawString("Stamina: " + stamina, 20, 70);
        imageHUD.drawString("Score: " + score, 20, 90);

        setImage(imageHUD);
    }

    /**
     * Gets the width of the HUD.
     * 
     * @return width of the HUD
     */
    public int getWidth() {
        return width;
    }

    /**
     * Gets the height of the HUD.
     * 
     * @return height of the HUD
     */
    public int getHeight() {
        return height;
    }
}
