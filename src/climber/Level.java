/**
 * Level
 * 
 * @author Project Group 13
 * @version 2012-01-09
 */
public class Level<MapT> {
    /**
     * the map
     */
    private final MapT map;

    /**
     * Class constructor.
     * 
     * @param map
     *            map
     */
    public Level(MapT map) {
        this.map = map;
    }

    /**
     * Gets the map of the level.
     * 
     * @return map
     */
    public MapT getMap() {
        return map;
    }
}
