/**
 * EnemyBirdnest
 * 
 * @author Project Group 13
 * @version 2012-01-09
 * @see Enemy
 */
public final class EnemyBirdnest extends Enemy {
    /**
     * timer for animation
     */
    private int timer = 0;

    /**
     * Class constructor.
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     * @param posX
     *            initial x-coordinate
     * @param posY
     *            initial y-coordinate
     */
    public EnemyBirdnest(String imageFileName, String imageFileName1, int posX,
            int posY) {
        super(imageFileName, imageFileName1);
        setPos(posX, posY);
    }

    /**
     * Class constructor. (sets the position by default)
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     */
    public EnemyBirdnest(String imageFileName, String imageFileName1) {
        super(imageFileName, imageFileName1);
        setPos(0, 0);
    }

    /**
     * Acts.
     * 
     * @see greenfoot.Actor#act()
     * @see Enemy#act()
     */
    @Override
    public void act() {
        super.act();

        timer++;
        if (timer == 20) {
            if (imageNum == 0) {
                setImage(imageFileName1);
                imageNum = 1;
            } else if (imageNum == 1) {
                setImage(imageFileName);
                imageNum = 0;
            }
            timer = 0;
        }

    }

    /**
     * Triggers the collision event.
     * 
     * @param player
     *            player
     */
    public static void triggerEvent(Player player) {
        player.setLife(player.getLife());
        player.setStamina(player.getStamina() - 1);
        player.setScore(player.getScore());

        player.startMessageTimer();
        player.setMessage("You hit a bird nest. Stamina lost!");

    }
}
