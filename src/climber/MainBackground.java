/**
 * MainBackground
 * 
 * @author Project Group 13
 * @version 2012-01-09
 * @see Background
 */
public final class MainBackground extends Background {
    /**
     * Class constructor.
     * 
     * @param imageFileName
     *            image file
     * @param width
     *            width of the main background
     * @param height
     *            height of the main background
     */
    public MainBackground(String imageFileName, int width, int height) {
        super(imageFileName, width, height);
    }

}
