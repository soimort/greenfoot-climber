import greenfoot.*;
import java.awt.Color;
import java.awt.Font;

/**
 * HUDMessage
 * 
 * @author Project Group 13
 * @version 2012-01-09
 */
public class HUDMessage extends Actor {
    /**
     * the width of the HUD message
     */
    private static int width = 260;
    /**
     * the height of the HUD message
     */
    private static int height = 25;
    /**
     * font size of the HUD message
     */
    private static float fontSize = 12.0f;

    /**
     * Constructor.
     * 
     * @param message
     *            message
     */
    public HUDMessage(String message) {
        update(message);
    }

    /**
     * Constructor. (by default)
     */
    public HUDMessage() {
        update("");
    }

    /**
     * Updates the HUD message.
     * 
     * @param message
     *            message
     * @param score
     *            score
     */
    public void update(String message) {
        GreenfootImage imageHUD = new GreenfootImage(width, height);

        if (message != "") {
            imageHUD.setColor(new Color(0, 0, 0, 128));
            imageHUD.fillRect(0, 0, width, height);

            Font font = imageHUD.getFont();
            font = font.deriveFont(fontSize);
            imageHUD.setFont(font);
            imageHUD.setColor(Color.WHITE);
            imageHUD.drawString(message, 15, 15);
        }

        setImage(imageHUD);
    }

    /**
     * Gets the width of the HUD message.
     * 
     * @return width of the HUD message
     */
    public int getWidth() {
        return width;
    }

    /**
     * Gets the height of the HUD message.
     * 
     * @return height of the HUD message
     */
    public int getHeight() {
        return height;
    }
}
