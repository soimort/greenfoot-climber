import greenfoot.Greenfoot;

/**
 * Player
 * 
 * @author Project Group 13
 * @version 2012-01-10
 * @see Climber
 */
public class Player extends Climber {
    /**
     * HUD message
     */
    private String message = "";
    /**
     * maximum time unit of showing message
     */
    private static int time_message = 50;
    /**
     * counter of time unit of showing message
     */
    private int counter_message = 0;

    /**
     * Class constructor.
     * 
     * @param imageFileName
     *            image file
     */
    public Player(String imageFileName) {
        super(imageFileName);
    }

    /**
     * Acts.
     * 
     * @see greenfoot.Actor#act()
     * @see Climber#act()
     */
    @Override
    public void act() {
        checkKeyPress();

        checkCollision();
        checkMessageTimer();

        super.act();
    }

    /**
     * Checks the keyboard.
     */
    private void checkKeyPress() {
        if (Greenfoot.isKeyDown("left") || Greenfoot.isKeyDown("a"))
            move(-1, 0);

        if (Greenfoot.isKeyDown("right") || Greenfoot.isKeyDown("d"))
            move(+1, 0);

        if ((Greenfoot.isKeyDown("up") || Greenfoot.isKeyDown("w")) && isStanding())
            startJumping(); // move(0, +1);

        if (Greenfoot.isKeyDown("down") || Greenfoot.isKeyDown("s"))
            move(0, -1);

        if (Greenfoot.isKeyDown("space") && isStanding())
            startJumping();
        
        if (Greenfoot.isKeyDown("z") )
            startJumping();
        
    }

    /**
     * Checks the collision.
     */
    private void checkCollision() {
        if (!getObjectsInRange(20, EnemyBirdnest.class).isEmpty())
            EnemyBirdnest.triggerEvent(this);

        if (!getObjectsInRange(20, EnemyBird.class).isEmpty())
            EnemyBird.triggerEvent(this);

        if (!getObjectsInRange(20, EnemyLizard.class).isEmpty())
            EnemyLizard.triggerEvent(this);

        if (!getObjectsInRange(20, EnemyStone.class).isEmpty())
            EnemyStone.triggerEvent(this);

        if (!getObjectsInRange(20, Coin.class).isEmpty()) {
            Coin coin = (Coin) getObjectsInRange(20, Coin.class).get(0);
            coin.triggerEvent(this);
        }

    }

    /**
     * Checks the message timer.
     */
    private void checkMessageTimer() {
        if (counter_message > 0)
            counter_message--;
        else
            setMessage("");
    }

    /**
     * Starts the message timer.
     */
    public void startMessageTimer() {
        counter_message = time_message;
    }

    /**
     * Gets the HUD message.
     * 
     * @return HUD message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the HUD message.
     * 
     * @param message
     *            HUD message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
