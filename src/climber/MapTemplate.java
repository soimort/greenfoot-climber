/**
 * MapTemplate
 * 
 * @author Project Group 13
 * @version 2012-01-09
 */
public abstract class MapTemplate {
    /**
     * image file of the background
     */
    protected String backgroundImage = "sky.png";
    /**
     * image file of the main background
     */
    protected String mainBackgroundImage = "main.png";
    /**
     * image file of the player (main character)
     */
    protected String playerImage = "player.png";
    /**
     * image file of platforms
     */
    protected String platformImage = "platform.png";
    /**
     * image file of birdnests
     */
    String birdnestImage = "enemy_birdnest.png";
    String birdnestImage1 = "enemy_birdnest1.png";
    /**
     * image file of birds
     */
    String birdImage = "enemy_bird.png";
    String birdImage1 = "enemy_bird1.png";
    String birdImage_r = "enemy_bird_r.png";
    String birdImage1_r = "enemy_bird1_r.png";
    /**
     * image file of lizards
     */
    String lizardImage = "enemy_lizard.png";
    String lizardImage1 = "enemy_lizard1.png";
    String lizardImage_r = "enemy_lizard_r.png";
    String lizardImage1_r = "enemy_lizard1_r.png";
    /**
     * image file of stones
     */
    String stoneImage = "enemy_stone.png";
    String stoneImage1 = "enemy_stone1.png";
    /**
     * image file of coins
     */
    String coinImage = "coin.png";

    /**
     * Initializes the map.
     * 
     * @param world
     *            world of the game
     */
    public abstract void init(ClimberWorld world);

    /**
     * Gets the file pathname.
     * 
     * @param mapId
     *            map ID
     * @param name
     *            name
     * @return file pathname
     */
    protected String getFileName(String mapId, String name) {
        return mapId + "/" + name;
    }
}
