import greenfoot.*;

/**
 * ClimberWorld
 * 
 * @author Project Group 13
 * @version 2012-01-09
 */
public final class ClimberWorld extends World {
    /**
     * the width of the viewport
     */
    public static int viewportWidth = 600;
    /**
     * the height of the viewport
     */
    public static int viewportHeight = 600;
    /**
     * the size of a cell
     */
    public static int cellSize = 1;
    /**
     * whether has boundary
     */
    public static boolean bounded = false;

    /**
     * the margin between the player and the bottom of the viewport
     */
    private static int playerBottomMargin = 100;

    /**
     * the width of the world (map)
     */
    private int worldWidth;
    /**
     * the height of the world (map)
     */
    private int worldHeight;

    /**
     * the main background in the level (map)
     */
    private MainBackground mainBackground;
    /**
     * the player (main character) in the level (map)
     */
    private Player player;
    /**
     * platforms in the level (map)
     */
    private Platform[] platforms;
    /**
     * birdnests in the level (map)
     */
    private EnemyBirdnest[] birdnests;
    /**
     * birds in the level (map)
     */
    private EnemyBird[] birds;
    /**
     * lizards in the level (map)
     */
    private EnemyLizard[] lizards;
    /**
     * stones in the level (map)
     */
    private EnemyStone[] stones;
    /**
     * coins in the level (map)
     */
    private Coin[] coins;

    /**
     * the HUD
     */
    private HUD hud;
    /**
     * the HUD message
     */
    private HUDMessage hudMessage;

    // the testing level (map)
    private Level<? extends MapTemplate> level_0;

    /**
     * Class constructor.
     */
    public ClimberWorld() {
        super(viewportWidth, viewportHeight, cellSize, bounded);

        // always paint the HUD and the player (main character) on the front
        setPaintOrder(HUD.class, HUDMessage.class, Player.class,
                EnemyBird.class, EnemyStone.class, EnemyBirdnest.class,
                EnemyLizard.class, Coin.class, Platform.class);

        // create the testing level (map)
        level_0 = new Level<MapDemo>(new MapDemo());
        level_0.getMap().init(this);

        // add the HUD
        hud = new HUD();
        super.addObject(hud, hud.getWidth() / 2, hud.getHeight() / 2);

        // add the HUD message
        hudMessage = new HUDMessage();
        super.addObject(hudMessage, viewportWidth / 2, viewportHeight
                - hudMessage.getHeight() / 2);

        updateCamera();
    }

    /**
     * Acts.
     * 
     * @see greenfoot.World#act()
     */
    @Override
    public void act() {
        updateCamera();
        checkGame();

        hud.update("Player", player.getLife(), player.getStamina(),
                player.getScore());
        hudMessage.update(player.getMessage());

    }

    /**
     * Adds object by Cartesian coordinates.
     * 
     * @param object
     *            object to be added
     * @param x
     *            x-coordinate
     * @param y
     *            y-coordinate
     */
    @Override
    public void addObject(Actor object, int x, int y) {
        super.addObject(object, x + worldWidth / 2, -y + worldHeight);
    }

    /**
     * Checks the game status (win or game over).
     */
    private void checkGame() {
        if (player.getPosY() < -10 || player.getLife() == 0) {
            player.setMessage("Game over!");
            Greenfoot.stop();

        } else if (player.getPosY() > worldHeight - 260) {
            player.setMessage("You win!");
            Greenfoot.stop();

        }
    }

    /**
     * Updates the viewport.
     */
    private void updateCamera() {
        // the player (main character) holds the constant position in the
        // viewport

        // move the main background in the viewport
        mainBackground.setLocation(-player.getPosX() + worldWidth / 2,
                player.getPosY() + viewportHeight - worldHeight + worldHeight
                        / 2 + 20);

        // move the platforms in the viewport
        for (Platform p : platforms) {
            p.setLocation(-player.getPosX() + p.getPosX(), player.getPosY()
                    + viewportHeight - worldHeight + p.getPosY());
        }

        // move the birdnests in the viewport
        if (birdnests != null) {
            for (EnemyBirdnest e : birdnests) {
                e.setLocation(-player.getPosX() + e.getPosX(), player.getPosY()
                        + viewportHeight - worldHeight + e.getPosY());
            }
        }

        // move the birds in the viewport
        if (birds != null) {
            for (EnemyBird e : birds) {
                e.setLocation(-player.getPosX() + e.getPosX(), player.getPosY()
                        + viewportHeight - worldHeight + e.getPosY());
            }
        }

        // move the lizards in the viewport
        if (lizards != null) {
            for (EnemyLizard e : lizards) {
                e.setLocation(-player.getPosX() + e.getPosX(), player.getPosY()
                        + viewportHeight - worldHeight + e.getPosY());
            }
        }

        // move the stones in the viewport
        if (stones != null) {
            for (EnemyStone e : stones) {
                e.setLocation(-player.getPosX() + e.getPosX(), player.getPosY()
                        + viewportHeight - worldHeight + e.getPosY());
            }
        }

        // move the coins in the viewport
        if (coins != null) {
            for (Coin c : coins) {
                c.setLocation(-player.getPosX() + c.getPosX(), player.getPosY()
                        + viewportHeight - worldHeight + c.getPosY());
            }
        }

    }

    /**
     * Gets the main background in the level (map).
     * 
     * @return main background
     */
    public MainBackground getMainBackground() {
        return mainBackground;
    }

    /**
     * Sets the main background in the level (map).
     * 
     * @param imageFileName
     *            image file
     * @param width
     *            width of the main background
     * @param height
     *            height of the main background
     */
    public void setMainBackground(String imageFileName, int width, int height) {
        mainBackground = new MainBackground(imageFileName, width, height);
        super.addObject(mainBackground, 0, 0);

        worldWidth = width;
        worldHeight = height;
    }

    /**
     * Gets the player (main character) in the level (map).
     * 
     * @return player (main character)
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Sets the player (main character) in the level (map).
     * 
     * @param imageFileName
     *            image file
     */
    public void setPlayer(String imageFileName) {
        player = new Player(imageFileName);
        super.addObject(player, viewportWidth / 2, viewportHeight
                - playerBottomMargin);
    }

    /**
     * Gets the platforms in the level (map).
     * 
     * @return platforms
     */
    public Platform[] getPlatforms() {
        return platforms;
    }

    /**
     * Sets the platforms in the level (map).
     * 
     * @param imageFileName
     *            image file
     * @param positions
     *            positions of platforms
     */
    public void setPlatforms(String imageFileName, int[][] positions) {
        platforms = new Platform[positions.length];
        for (int i = 0; i < positions.length; i++) {
            platforms[i] = new Platform(imageFileName, positions[i][0],
                    positions[i][1]);
            super.addObject(platforms[i], 0, 0);
        }
    }

    /**
     * Gets the birdnests in the level (map).
     * 
     * @return birdnests
     */
    public EnemyBirdnest[] getBirdnests() {
        return birdnests;
    }

    /**
     * Sets the birdnests in the level (map)
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     * @param positions
     *            positions of birdnests
     */
    public void setBirdnests(String imageFileName, String imageFileName1,
            int[][] positions) {
        birdnests = new EnemyBirdnest[positions.length];
        for (int i = 0; i < positions.length; i++) {
            birdnests[i] = new EnemyBirdnest(imageFileName, imageFileName1,
                    positions[i][0], positions[i][1]);
            super.addObject(birdnests[i], 0, 0);
        }
    }

    /**
     * Gets the birds in the level (map).
     * 
     * @return birds
     */
    public EnemyBird[] getBirds() {
        return birds;
    }

    /**
     * Sets the birds in the level (map)
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     * @param imageFileName_r
     *            image file
     * @param imageFileName1_r
     *            image file
     * @param positions
     *            positions of birds
     */
    public void setBirds(String imageFileName, String imageFileName1,
            String imageFileName_r, String imageFileName1_r, int[][] positions) {
        birds = new EnemyBird[positions.length];
        for (int i = 0; i < positions.length; i++) {
            birds[i] = new EnemyBird(imageFileName, imageFileName1,
                    imageFileName_r, imageFileName1_r, positions[i][0],
                    positions[i][1]);
            super.addObject(birds[i], 0, 0);
        }
    }

    /**
     * Gets the lizards in the level (map).
     * 
     * @return lizards
     */
    public EnemyLizard[] getLizards() {
        return lizards;
    }

    /**
     * Sets the lizards in the level (map)
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     * @param imageFileName_r
     *            image file
     * @param imageFileName1_r
     *            image file
     * @param positions
     *            positions of lizards
     */
    public void setLizards(String imageFileName, String imageFileName1,
            String imageFileName_r, String imageFileName1_r, int[][] positions) {
        lizards = new EnemyLizard[positions.length];
        for (int i = 0; i < positions.length; i++) {
            lizards[i] = new EnemyLizard(imageFileName, imageFileName1,
                    imageFileName_r, imageFileName1_r, positions[i][0],
                    positions[i][1]);
            super.addObject(lizards[i], 0, 0);
        }
    }

    /**
     * Gets the stones in the level (map).
     * 
     * @return stones
     */
    public EnemyStone[] getStones() {
        return stones;
    }

    /**
     * Sets the stones in the level (map)
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     * @param positions
     *            positions of stones
     */
    public void setStones(String imageFileName, String imageFileName1,
            int[][] positions) {
        stones = new EnemyStone[positions.length];
        for (int i = 0; i < positions.length; i++) {
            stones[i] = new EnemyStone(imageFileName, imageFileName1,
                    positions[i][0], positions[i][1]);
            super.addObject(stones[i], 0, 0);
        }
    }

    /**
     * Gets the coins in the level (map).
     * 
     * @return coins
     */
    public Coin[] getCoins() {
        return coins;
    }

    /**
     * Sets the coins in the level (map).
     * 
     * @param imageFileName
     *            image file
     * @param positions
     *            positions of coins
     */
    public void setCoins(String imageFileName, int[][] positions) {
        coins = new Coin[positions.length];
        for (int i = 0; i < positions.length; i++) {
            coins[i] = new Coin(imageFileName, positions[i][0], positions[i][1]);
            super.addObject(coins[i], 0, 0);
        }
    }

}
