import greenfoot.*;

/**
 * Player
 * 
 * @author Project Group 13
 * @version 2012-01-09
 */
public class Platform extends Actor {
    /**
     * the Cartesian position x-coordinate
     */
    private int posX = 0;
    /**
     * the Cartesian position y-coordinate
     */
    private int posY = 0;

    /**
     * Class constructor.
     * 
     * @param imageFileName
     *            image file
     * @param posX
     *            initial x-coordinate
     * @param posY
     *            initial y-coordinate
     */
    public Platform(String imageFileName, int posX, int posY) {
        setImage(imageFileName);
        setPos(posX, posY);
    }

    /**
     * Class constructor. (sets the position by default)
     * 
     * @param imageFileName
     *            image file
     */
    public Platform(String imageFileName) {
        setImage(imageFileName);
        setPos(0, 0);
    }

    /**
     * Gets the Cartesian position x-coordinate.
     * 
     * @return x-coordinate
     */
    public int getPosX() {
        return posX;
    }

    /**
     * Sets the Cartesian position x-coordinate.
     * 
     * @param posX
     *            x-coordinate
     */
    public void setPosX(int posX) {
        this.posX = posX;
    }

    /**
     * Gets the Cartesian position y-coordinate.
     * 
     * @return y-coordinate
     */
    public int getPosY() {
        return posY;
    }

    /**
     * Sets the Cartesian position y-coordinate.
     * 
     * @param posY
     *            y-coordinate
     */
    public void setPosY(int posY) {
        this.posY = posY;
    }

    /**
     * Sets the Cartesian position.
     * 
     * @param posX
     *            x-coordinate
     * @param posY
     *            y-coordinate
     */
    public void setPos(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }
}
