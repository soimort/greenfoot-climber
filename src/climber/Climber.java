import greenfoot.*;

/**
 * Climber
 * 
 * @author Project Group 13
 * @version 2012-01-10
 */
public abstract class Climber extends Actor {
    /**
     * the Cartesian position x-coordinate
     */
    protected int posX = 0;
    /**
     * the Cartesian position y-coordinate
     */
    protected int posY = 0;

    /**
     * the velocity x-component
     */
    protected int vX = 0;
    /**
     * the velocity y-component
     */
    protected int vY = 0;

    /**
     * the velocity of falling y-component
     */
    protected static int vY_falling = -10;
    /**
     * the velocity of jumping y-component
     */
    protected static int vY_jumping = 8;

    /**
     * maximum time unit of jumping
     */
    protected static int time_jumping = 15;
    /**
     * counter of time unit of jumping
     */
    protected int counter_jumping = 0;

    /**
     * maximum time unit of landing
     */
    protected static int time_landing = 5;
    /**
     * counter of time unit of landing
     */
    protected int counter_landing = 0;

    /**
     * speed of move by displacement
     */
    protected static int moveSpeed = 4;

    /**
     * image file
     */
    protected String imageFileName;

    /**
     * life of the climber
     */
    private int life = 100;
    /**
     * stamina of the climber
     */
    private int stamina = 100;
    /**
     * score of the climber
     */
    private int score = 0;

    /**
     * Class constructor.
     * 
     * @param imageFileName
     *            image file
     */
    public Climber(String imageFileName) {
        this.imageFileName = imageFileName;
        setImage(imageFileName);
    }

    /**
     * Acts.
     * 
     * @see greenfoot.Actor#act()
     */
    @Override
    public void act() {
        // check jumping first, then falling, lastly landing
        checkJumping();
        checkFalling();
        checkLanding();

        // renew the position by the velocity
        posX += vX * 1;
        posY += vY * 1;
    }

    /**
     * Moves by displacement.
     * 
     * @param dX
     *            displacement x-component
     * @param dY
     *            displacement y-component
     * @param moveSpeed
     *            speed of move by displacement
     */
    protected void move(int dX, int dY, int moveSpeed) {
        setPosX(posX + dX * moveSpeed);
        setPosY(posY + dY * moveSpeed);
    }

    /**
     * Moves by displacement. (at default speed)
     * 
     * @param dX
     *            displacement x-component
     * @param dY
     *            displacement y-component
     */
    protected void move(int dX, int dY) {
        setPosX(posX + dX * moveSpeed);
        setPosY(posY + dY * moveSpeed);
    }

    /**
     * Shows if on a platform.
     * 
     * @return true if on a platform
     */
    protected boolean isOnPlatform() {
        return !getIntersectingObjects(Platform.class).isEmpty();
    }

    /**
     * Shows if standing.
     * 
     * @return true if standing
     */
    protected boolean isStanding() {
        if (counter_landing > 0)
            counter_landing--;
        return vY == 0 && counter_landing == 0;
    }

    /**
     * Shows if falling.
     * 
     * @return true if falling
     */
    protected boolean isFalling() {
        return vY == vY_falling;
    }

    /**
     * Shows if jumping.
     * 
     * @return true if jumping
     */
    protected boolean isJumping() {
        return vY == vY_jumping;
    }

    /**
     * Starts jumping.
     */
    protected void startJumping() {
        vY = vY_jumping;

        counter_jumping = time_jumping;
    }

    /**
     * Checks if landing on a platform.
     */
    protected void checkLanding() {
        if (isFalling() && isOnPlatform()) {
            vY = 0;

            counter_landing = time_landing;
        }
    }

    /**
     * Checks if falling.
     */
    protected void checkFalling() {
        if (!isOnPlatform() && !isJumping())
            vY = vY_falling;
    }

    /**
     * Checks if jumping.
     */
    protected void checkJumping() {
        if (isJumping())
            if (counter_jumping > 0)
                counter_jumping--;
            else
                vY = 0;
    }

    /**
     * Gets the Cartesian position x-coordinate.
     * 
     * @return x-coordinate
     */
    public final int getPosX() {
        return posX;
    }

    /**
     * Sets the Cartesian position x-coordinate.
     * 
     * @param posX
     *            x-coordinate
     */
    public final void setPosX(int posX) {
        this.posX = posX;
    }

    /**
     * Gets the Cartesian position y-coordinate.
     * 
     * @return y-coordinate
     */
    public final int getPosY() {
        return posY;
    }

    /**
     * Sets the Cartesian position y-coordinate.
     * 
     * @param posY
     *            y-coordinate
     */
    public final void setPosY(int posY) {
        this.posY = posY;
    }

    /**
     * Sets the Cartesian position.
     * 
     * @param posX
     *            x-coordinate
     * @param posY
     *            y-coordinate
     */
    public final void setPos(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    /**
     * Gets the velocity x-component.
     * 
     * @return x-component
     */
    public final int getVX() {
        return vX;
    }

    /**
     * Sets the velocity x-component.
     * 
     * @param vX
     *            x-component
     */
    public final void setVX(int vX) {
        this.vX = vX;
    }

    /**
     * Gets the velocity y-component.
     * 
     * @return y-component
     */
    public final int getVY() {
        return vY;
    }

    /**
     * Sets the velocity y-component.
     * 
     * @param vY
     *            y-component
     */
    public final void setVY(int vY) {
        this.vY = vY;
    }

    /**
     * Sets the velocity.
     * 
     * @param vX
     *            x-component
     * @param vY
     *            y-component
     */
    public final void setV(int vX, int vY) {
        this.vX = vX;
        this.vY = vY;
    }

    /**
     * Gets the image file pathname.
     * 
     * @return image file
     */
    public final String getImageFileName() {
        return imageFileName;
    }

    /**
     * Gets the life of the climber.
     * 
     * @return life
     */
    public int getLife() {
        return life;
    }

    /**
     * Sets the life of the climber
     * 
     * @param life
     */
    public void setLife(int life) {
        if (life > 0)
            this.life = life;
        else
            this.life = 0;
    }

    /**
     * Gets the stamina of the climber.
     * 
     * @return stamina
     */
    public int getStamina() {
        return stamina;
    }

    /**
     * Sets the stamina of the climber.
     * 
     * @param stamina
     */
    public void setStamina(int stamina) {
        if (stamina > 0)
            this.stamina = stamina;
        else
            this.stamina = 0;
    }

    /**
     * Gets the score of the climber.
     * 
     * @return score
     */
    public int getScore() {
        return score;
    }

    /**
     * Sets the score of the climber.
     * 
     * @param score
     */
    public void setScore(int score) {
        this.score = score;
    }
}
