/**
 * EnemyBird
 * 
 * @author Project Group 13
 * @version 2012-01-09
 * @see Enemy
 */
public final class EnemyBird extends Enemy {
    /**
     * timer for animation
     */
    private int timer = 0;

    /**
     * Class constructor.
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     * @param imageFileName_r
     *            image file
     * @param imageFileName1_r
     *            image file
     * @param posX
     *            initial x-coordinate
     * @param posY
     *            initial y-coordinate
     */
    public EnemyBird(String imageFileName, String imageFileName1,
            String imageFileName_r, String imageFileName1_r, int posX, int posY) {
        super(imageFileName, imageFileName1, imageFileName_r, imageFileName1_r);
        setPos(posX, posY);

        vX = 2;
    }

    /**
     * Class constructor. (sets the position by default)
     * 
     * @param imageFileName
     *            image file
     * @param imageFileName1
     *            image file
     * @param imageFileName_r
     *            image file
     * @param imageFileName1_r
     *            image file
     */
    public EnemyBird(String imageFileName, String imageFileName1,
            String imageFileName_r, String imageFileName1_r) {
        super(imageFileName, imageFileName1, imageFileName_r, imageFileName1_r);
        setPos(0, 0);

        vX = 2;
    }

    /**
     * Acts.
     * 
     * @see greenfoot.Actor#act()
     * @see Enemy#act()
     */
    @Override
    public void act() {
        super.act();

        timer++;
        if (timer == 20) {
            if (imageNum == 0) {
                setImage(imageFileName1);
                imageNum = 1;
            } else if (imageNum == 1) {
                setImage(imageFileName);
                imageNum = 0;
            } else if (imageNum == 2) {
                setImage(imageFileName1_r);
                imageNum = 3;
            } else if (imageNum == 3) {
                setImage(imageFileName_r);
                imageNum = 2;
            }
            timer = 0;
        }

        if (posX < 0) {
            vX = -vX;
            setImage(imageFileName);
            imageNum = 0;
        } else if (posX > ClimberWorld.viewportWidth) {
            vX = -vX;
            setImage(imageFileName_r);
            imageNum = 2;
        }
    }

    /**
     * Triggers the collision event.
     * 
     * @param player
     *            player
     */
    public static void triggerEvent(Player player) {
        player.setLife(player.getLife() - 1);
        player.setStamina(player.getStamina() - 1);
        player.setScore(player.getScore());

        player.startMessageTimer();
        player.setMessage("You hit a bird. Life and stamina lost!");
    }
}
