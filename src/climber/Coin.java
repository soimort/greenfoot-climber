import greenfoot.GreenfootImage;

/**
 * Coin
 * 
 * @author Project Group 13
 * @version 2012-01-09
 * @see EnvironmentObject
 */
public final class Coin extends EnvironmentObject {
    /**
     * whether the coin has been picked
     */
    private boolean picked = false;

    /**
     * Class constructor.
     * 
     * @param imageFileName
     *            image file
     * @param posX
     *            initial x-coordinate
     * @param posY
     *            initial y-coordinate
     */
    public Coin(String imageFileName, int posX, int posY) {
        super(imageFileName, imageFileName);
        setPos(posX, posY);
    }

    /**
     * Acts.
     * 
     * @see greenfoot.Actor#act()
     * @see EnvironmentObject#act()
     */
    @Override
    public void act() {
        super.act();
    }

    /**
     * Triggers the collision event.
     * 
     * @param player
     *            player
     */
    public void triggerEvent(Player player) {
        if (!picked) {
            player.setLife(player.getLife());
            player.setStamina(player.getStamina());
            player.setScore(player.getScore() + 10);

            player.startMessageTimer();
            player.setMessage("You picked up a coin!");

            setImage(new GreenfootImage(1, 1));
            picked = true;
        }
    }
}
